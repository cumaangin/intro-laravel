<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/* TUGAS1 1 */
Route::get('/','HomeController@home');
Route::post('/welcome','AuthController@welcome');
Route::get('/register','AuthController@register');

/* TUGAS 2 */
Route::get('/table', function(){
    return view('tugas2.tables.table');
});
Route::get('/data-tables', function(){
    return view('tugas2.tables.data-tables');
});
/* TUGAS 3 */
Route::get('/cast','CastController@index');
Route::get('/cast/create','CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');