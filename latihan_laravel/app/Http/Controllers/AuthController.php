<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('tugas1.register');
    }
    public function welcome(Request $req){
        //return view('welcome');
        $first_name = $req['first_name'];
        $last_name = $req['last_name'];
        
        return view('tugas1.welcome', compact('first_name','last_name'));
        /// compact untuk melempar data/variabel yang sebelumnya sudah direquest ke welcome (karena return view ke welcome.blade.php)
    }
}
