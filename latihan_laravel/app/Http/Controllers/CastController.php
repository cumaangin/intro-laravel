<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    /* CREATE */
    public function create()
    {
        return view('cast.create');
    }
    /* READ */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        //return redirect('/cast/create');
        /* diubah agar perubahannya terjadi pada url /cast saja */
        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', compact('cast'));
        /* compact itu berfungsi untuk melempar data yang sudah didapat, dalam hal ini data dari table cast. maka nama tablenya lah yang dicompact */
    }
    /* SHOW ATAU TAMPIL DATA */
    public function show($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.show',compact('cast'));
    }
    /* EDIT */
    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('cast'));
    }
        /* UPDATE */
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request["nama"],
                'umur' => $request["umur"],
                'bio' => $request["bio"]
            ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        $query = DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
