@extends('layout.master')
@section('title')
Ayo Buat Account Baru !
@endsection
@section('title2')
Sign Up Form
@endsection
@section('konten')
<form action="/welcome" method="POST">
    @csrf
    <label style="margin-right: 10px;">First Name :</label><br><br><input type="text" name="first_name" style="margin-top: 5px;"><br><br>
    <label>Last Name :</label><br><input type="text" name="last_name" style="margin-top: 5px;"><br><br>
    <label>Gender :</label><br><br><input type="radio" name="gender" value="male">Male<br>
    <input type="radio" name="gender" value="female">Female<br>
    <input type="radio" name="gender" value="prefer_not_to_say">Prefer not to say<br><br>
    <label>Nationality : </label><br><br>
    <select name="nation">
        <option value="opsi">-- Nationality --</option>
        <option value="indo">Indonesian</option>
        <option value="malay">Malaysian</option>
        <option value="aussie">Aussie</option>
        <option value="other">Other</option>
    </select><br><br>
    <label>Language Spoken : </label><br><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Japanese<br>
    <input type="checkbox">Others<br><br>
    <label>Bio :</label><br><br>
    <textarea name="bio" id="" cols="35" rows="10" placeholder="Type your bio"></textarea><br>
    <button type="submit">Sign Up</button>
</form>
@endsection
        