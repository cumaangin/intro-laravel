@extends('layout.master')
@section('title')
    SanberBook
@endsection
@section('title2')
    Halaman Utama
@endsection
@section('konten')
<h1>Social Media Developer Santai Berkualitas</h1>
<p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
<h2>Benefit Join di SanberBook</h2>
<ul>
    <li>Mendapatkan motivasi dari sesama developer</li>
    <li>Sharing knowledge dari para mastah sanber</li>
    <li>Dibuat oleh calon web developer terbaik</li>
</ul>
<h2>Cara Bergabung ke SanberBook</h2>
<ol>
    <li>Mengunjungi Website ini</li>
    <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
    <li>Selesai!</li>
</ol>
@endsection