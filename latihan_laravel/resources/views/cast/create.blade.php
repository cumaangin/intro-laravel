@extends('layout.master')

@section('title1')
<h2>Tambah Data</h2>
@endsection
@section('title2')
<h2>Tambah Data Cast</h2>
@endsection
@section('konten')  
<div>
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="title">Nama Cast : </label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Cast :">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur : </label>
            <input type="text" class="form-control" name="umur" id="umur" placeholder="Umur :">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio</label>
            <textarea name="bio" class="form-control" cols=10" rows="10" placeholder="Tuliskan bio mu :"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection