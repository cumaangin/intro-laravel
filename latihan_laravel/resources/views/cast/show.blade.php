@extends('layout.master')
@section('title1')
    Tampil data cast
@endsection
@section('title2')
    Tampil data cast : {{$cast->nama}}
@endsection
@section('konten')
    <h3>Nama Cast : {{$cast->nama}}</h3>
    <p>Umur: {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>
@endsection