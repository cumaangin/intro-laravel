@extends('layout.master')
@section('title1')
    Update data
@endsection
@section('title2')
    Edit data {{$cast->nama}}
@endsection
@section('konten')
<div>
    <h2>Edit cast {{$cast->nama}}</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Nama Cast : </label>
            <input type="text" class="form-control" value="{{$cast->nama}}" name="nama" id="nama" placeholder="Edit Nama Cast :">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur : </label>
            <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" id="umur" placeholder="Edit Umur :">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Bio</label>
            <textarea name="bio" class="form-control" cols=10" rows="10" placeholder="Edit bio mu :">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection